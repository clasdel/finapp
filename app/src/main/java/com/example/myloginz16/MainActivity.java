package com.example.myloginz16;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements  Comunicacion{

    private TextView txtUsuario, txtPassword;
    private Button btnIngresar;
    private ProgressBar pgbEjecucion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        txtUsuario = findViewById(R.id.txtUserName);
        txtPassword = findViewById(R.id.txtPass);
        btnIngresar = findViewById(R.id.btnAceptar);
        pgbEjecucion = findViewById(R.id.pbgEjecutando);


        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TareaAsincrona(MainActivity.this).execute
                        (txtUsuario.getText().toString(),
                        txtPassword.getText().toString(), 3000);
            }
        });

    }

    @Override
    public void toggleProgressBar(boolean status) {
        if (status){
            pgbEjecucion.setVisibility(View.VISIBLE);
        }else {
            pgbEjecucion.setVisibility(View.GONE);
        }
    }

    @Override
    public void lanzarActividad(Class<?> tipoActividad) {
        Intent intent = new Intent(this, tipoActividad);
        startActivity(intent);
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(this, msg,
                Toast.LENGTH_SHORT).show();
    }

    public void ejecutarInfo(View view){
        Intent i = new Intent(this, InfoActivity.class);
        startActivity(i);
    }


}