package com.example.finapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.finapp.db.DbHelper;
import com.example.finapp.db.DbUsuarios;
import com.example.finapp.entidades.Usuarios;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    EditText us, pass;
    Button btnEntrar, btnRegistro, btnBorrar;
    DbUsuarios usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        us = findViewById(R.id.txtUserName);
        pass = findViewById(R.id.txtPass);
        btnEntrar = findViewById(R.id.btnIngresar);
        btnRegistro = findViewById(R.id.btnRegistrarse);
        btnBorrar = findViewById(R.id.btnCancelar);

        btnEntrar.setOnClickListener(this);
        btnRegistro.setOnClickListener(this);
        btnBorrar.setOnClickListener(this);
        usuario = new DbUsuarios(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnIngresar:
                String user = us.getText().toString();
                String password = pass.getText().toString();
                if (user.equals("") && password.equals("")){
                    Toast.makeText(this, "ERROR: Campos vacíos. ", Toast.LENGTH_LONG).show();
                }else if (usuario.login(user, password) == 1){
                    Usuarios ux = usuario.obtenerUsuario(user, password);
                    Toast.makeText(this, "Credenciales Correctas. ", Toast.LENGTH_LONG).show();
                    Intent a = new Intent(MainActivity.this,Inicio.class);
                    a.putExtra("id", ux.getId());
                    startActivity(a);
                }else{
                    Toast.makeText(this, "ERROR: Credenciales incorrectas. ", Toast.LENGTH_LONG).show();
                }


                break;

            case R.id.btnRegistrarse:

                Intent i = new Intent(MainActivity.this,Registrar.class);
                startActivity(i);

                break;

            case R.id.btnCancelar:

                limpiar();

                break;

        }
    }

    public void limpiar(){
        us.setText("");
        pass.setText("");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}