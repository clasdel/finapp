package com.example.finapp.model;

public class Banco {

    String nombre, direccion, ciudad, barrio;

    public Banco() {
    }

    public Banco(String nombre, String direccion, String ciudad, String barrio) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.barrio = barrio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }
}
