package com.example.finapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.finapp.adapter.BancosAdapter;
import com.example.finapp.model.Banco;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class ConsultarBancos extends AppCompatActivity {

    RecyclerView myPlaces;
    BancosAdapter myAdapter;
    FirebaseFirestore myFirestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar_bancos);

        myFirestore = FirebaseFirestore.getInstance();
        myPlaces = findViewById(R.id.listaBancos);
        myPlaces.setLayoutManager(new LinearLayoutManager(this));

        Query query = myFirestore.collection("Bancos");

        FirestoreRecyclerOptions<Banco> firestoreRecyclerOptions =
                new FirestoreRecyclerOptions.Builder<Banco>().setQuery(query, Banco.class).build();

        myAdapter = new BancosAdapter(firestoreRecyclerOptions, this);
        myAdapter.notifyDataSetChanged();
        myPlaces.setAdapter(myAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        myAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        myAdapter.stopListening();
    }
}