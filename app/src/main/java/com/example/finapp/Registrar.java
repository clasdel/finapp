package com.example.finapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.finapp.db.DbUsuarios;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class Registrar extends AppCompatActivity implements View.OnClickListener{

    EditText nombre, apellido, correo,password;
    Button btnGuardarRegistro, btnCancelarRegistro;
    private FirebaseFirestore myFirestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);

        nombre = findViewById(R.id.txtRegNombre);
        apellido = findViewById(R.id.txtRegApellido);
        correo = findViewById(R.id.txtRegCorreo);
        password = findViewById(R.id.txtRegPassword);
        btnGuardarRegistro = findViewById(R.id.btnRegRegistrarBanco);
        btnCancelarRegistro = findViewById(R.id.btnRegCancelarBanco);
        myFirestore = FirebaseFirestore.getInstance();

        btnGuardarRegistro.setOnClickListener(this);
        btnCancelarRegistro.setOnClickListener(this);

    }

    @Override
    public void onClick(View regView) {

        switch (regView.getId()){
            case R.id.btnRegRegistrarBanco:
                DbUsuarios dbUsuarios = new DbUsuarios(Registrar.this);
                long id = dbUsuarios.insertarUsuario(
                        nombre.getText().toString(),
                        apellido.getText().toString(),
                        correo.getText().toString(),
                        password.getText().toString());
                String nom = nombre.getText().toString();
                String ap = apellido.getText().toString();
                String cor = correo.getText().toString();
                String pas = password.getText().toString();
                if (nom.isEmpty() && ap.isEmpty() && cor.isEmpty() && pas.isEmpty()){
                    Toast.makeText(this, "ERROR: Algunos campos estan vacíos", Toast.LENGTH_LONG).show();
                }else if (id > 0){
                    postUsuario(nom, ap, cor, pas);
                    Toast.makeText(this, "¡¡Registrado Existosamente!!", Toast.LENGTH_LONG).show();
                    limpiar();
                }else {
                    Toast.makeText(this, "Ya existe un usuario con este correo", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.btnRegCancelarBanco:
                limpiar();
                break;
        }

    }

    private void postUsuario(String nom, String ap, String cor, String pas) {
        Map<String, Object> map = new HashMap<>();
        map.put("nombre", nom);
        map.put("apellido", ap);
        map.put("correo", cor);
        map.put("contraseña", pas);

        myFirestore.collection("Usuarios").add(map).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                Toast.makeText(Registrar.this, "Banco Registrado", Toast.LENGTH_LONG).show();
                finish();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(Registrar.this, "Error al registrar", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void limpiar(){
        nombre.setText("");
        apellido.setText("");
        correo.setText("");
        password.setText("");
    }


}


