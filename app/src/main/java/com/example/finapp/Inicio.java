package com.example.finapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

public class Inicio extends AppCompatActivity{

    FloatingActionButton fbNavegar;
    Button btnRegistroBC;
    TextView saludo;

    DatabaseReference myDatabaseReference = FirebaseDatabase.getInstance().getReference();
    DatabaseReference myRootChild = myDatabaseReference.child("texto");

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        fbNavegar = findViewById(R.id.fbNavigation);
        btnRegistroBC = findViewById(R.id.btnRegBancoOCorr);
        saludo = findViewById(R.id.textViewSaludo);

        fbNavegar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nav = new Intent(Inicio.this, MapsActivity.class);
                startActivity(nav);
            }
        });

        btnRegistroBC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent reg = new Intent(Inicio.this, RegistrarSedes.class);
                startActivity(reg);
            }
        });

        WebView myWebView = (WebView) findViewById(R.id.webview);
        myWebView.loadUrl("https://www.google.com/finance/?sa=X&ved=2ahUKEwiuyonlqaX7AhXoSjABHSgcC5AQ_AUoA3oECAEQDQ");

     }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_configuracion, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case(R.id.configuracion):
                ejecutarAjustes();
                break;

            case(R.id.cerrarSesion):
                Intent salir = new Intent(Inicio.this, MainActivity.class);
                startActivity(salir);
                break;

            case(R.id.action_info):
                Intent info = new Intent(Inicio.this, InfoApp.class);
                startActivity(info);
                break;

        }//cerramos  switch
        return super.onOptionsItemSelected(item);
    }

    private void ejecutarAjustes(){
        Intent o = new Intent(Inicio.this, Ajustes.class);
        startActivity(o);
    }

    @Override
    protected void onStart() {
        super.onStart();
        myRootChild.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                String texto = Objects.requireNonNull(snapshot.getValue().toString());
                saludo.setText(texto);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

}