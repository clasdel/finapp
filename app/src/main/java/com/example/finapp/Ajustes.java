package com.example.finapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Ajustes extends AppCompatActivity implements View.OnClickListener {

    Button bCrear, bConsultar, bBancos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajustes);

        bCrear = findViewById(R.id.btnAjustesCrear);
        bConsultar = findViewById(R.id.btnAjustesConsultar);
        bBancos = findViewById(R.id.btnAjustesConsultarBancos);

        bCrear.setOnClickListener(this);
        bConsultar.setOnClickListener(this);
        bBancos.setOnClickListener(this);

    }

    @Override
    public void onClick(View ajustesView) {
        switch (ajustesView.getId()){
            case R.id.btnAjustesCrear:
                Intent c = new Intent(Ajustes.this,Registrar.class);
                startActivity(c);
                break;
            case R.id.btnAjustesConsultar:
                Intent r = new Intent(Ajustes.this,Consultar.class);
                startActivity(r);
                break;
            case R.id.btnAjustesConsultarBancos:
                Intent b = new Intent(Ajustes.this, ConsultarBancos.class);
                startActivity(b);
        }

    }
}