package com.example.finapp.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finapp.R;
import com.example.finapp.model.Banco;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class BancosAdapter extends FirestoreRecyclerAdapter<Banco, BancosAdapter.ViewHolder> {

    private FirebaseFirestore myFirestore = FirebaseFirestore.getInstance();
    Activity activity;


    /**
     * Create a new RecyclerView adapter that listens to a Firestore Query.  See {@link
     * FirestoreRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public BancosAdapter(@NonNull FirestoreRecyclerOptions<Banco> options, Activity activity) {
        super(options);
        this.activity = activity;
    }

    @Override
    protected void onBindViewHolder(@NonNull BancosAdapter.ViewHolder holder, int position, @NonNull Banco banco) {
        DocumentSnapshot documentSnapshot = getSnapshots().getSnapshot(holder.getAdapterPosition());
        final String id = documentSnapshot.getId();

        holder.nombre.setText(banco.getNombre());
        holder.direccion.setText(banco.getDireccion());
        holder.ciudad.setText(banco.getCiudad());
        holder.barrio.setText(banco.getBarrio());

        holder.btnDeleteBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteUsuario(id);
            }
        });

    }

    @NonNull
    @Override
    public BancosAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_bancos_single, parent, false);
        return new ViewHolder(v);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView nombre, direccion, ciudad, barrio;
        ImageButton btnDeleteBank;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nombre = itemView.findViewById(R.id.txtNombreSingleBan);
            direccion = itemView.findViewById(R.id.txtDirSingleBan);
            ciudad = itemView.findViewById(R.id.txtCiudadSingle);
            barrio = itemView.findViewById(R.id.txtBarrioSingle);
            btnDeleteBank = itemView.findViewById(R.id.btnEliminarBanco);
        }
    }

    private void deleteUsuario(String id){
        myFirestore.collection("Bancos").document(id).delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                        Toast.makeText(activity, "Eliminado Correctamente ", Toast.LENGTH_LONG).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(activity, "No se pudo eliminar", Toast.LENGTH_LONG).show();
                    }
                });
    }
}
