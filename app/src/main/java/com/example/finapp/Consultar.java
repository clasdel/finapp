package com.example.finapp;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finapp.adaptadores.ListaUsuariosAdapter;
import com.example.finapp.db.DbUsuarios;
import com.example.finapp.entidades.Usuarios;

import java.util.ArrayList;

public class Consultar extends AppCompatActivity {

    RecyclerView listarUsuarios;
    ArrayList<Usuarios> listaArrayUsuarios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar);

        listarUsuarios = findViewById(R.id.listaContactos);
        listarUsuarios.setLayoutManager(new LinearLayoutManager(this));

        DbUsuarios dbUsuarios = new DbUsuarios(Consultar.this);
        listaArrayUsuarios = new ArrayList<>();

        ListaUsuariosAdapter adapter = new ListaUsuariosAdapter(dbUsuarios.mostrarUsuarios());
        listarUsuarios.setAdapter(adapter);

    }
}
