package com.example.finapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.finapp.db.DbUsuarios;
import com.example.finapp.entidades.Usuarios;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class EditarPerfiles extends AppCompatActivity  {

    EditText txtNombre, txtApellido, txtCorreo, txtPass;
    Button btnGuardar, btnCancelar;
    FloatingActionButton fbEditar,fbEliminar;
    TextView advertencia;

    Usuarios usuario;
    int id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_perfiles);

        txtNombre = findViewById(R.id.edtNombre);
        txtApellido = findViewById(R.id.edtApellido);
        txtCorreo = findViewById(R.id.edtCorreo);
        txtPass = findViewById(R.id.edtPassword);
        btnGuardar = findViewById(R.id.btnEdGuardarCambios);
        btnCancelar = findViewById(R.id.btnEdCancelarCambios);
        fbEditar = findViewById(R.id.fabEditar);
        fbEliminar = findViewById(R.id.fabEliminar);
        advertencia = findViewById(R.id.textAdvertenciaEdit);


        if (savedInstanceState == null){
            Bundle extras = getIntent().getExtras();

            if (extras == null){
                id = Integer.parseInt(null);
            }else {
                id = extras.getInt("ID");
            }
        }else {
            id = (int) savedInstanceState.getSerializable("ID");
        }

        DbUsuarios dbUsuarios = new DbUsuarios(EditarPerfiles.this);
        usuario = dbUsuarios.verUsuario(id);

        if (usuario != null){
            txtNombre.setText(usuario.getNombre());
            txtApellido.setText(usuario.getApellido());
            txtCorreo.setText(usuario.getCorreo());
            txtPass.setText(usuario.getCorreo());

            btnGuardar.setVisibility(View.INVISIBLE);
            btnCancelar.setVisibility(View.INVISIBLE);
            advertencia.setVisibility(View.INVISIBLE);
            txtNombre.setInputType(InputType.TYPE_NULL);
            txtApellido.setInputType(InputType.TYPE_NULL);
            txtCorreo.setInputType(InputType.TYPE_NULL);
            txtPass.setInputType(InputType.TYPE_NULL);

        }

        fbEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EditarPerfiles.this, EditarDatosCuenta.class);
                intent.putExtra("ID", id);
                startActivity(intent);
            }
        });

        fbEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(EditarPerfiles.this);
                dialog.setMessage("¿Estas seguro de eliminar este Usuario?");
                dialog.setCancelable(true);
                dialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dbUsuarios.eliminarUsuario(id);
                        Toast.makeText(EditarPerfiles.this, "Usuario Eliminado", Toast.LENGTH_LONG).show();
                        verRegistros();
                }
            });
                dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // no hacemos nada
                            }
                        })
                        .show();
            }
        });

    }

    public void verRegistros(){
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("ID", id);
        startActivity(intent);
    }

}