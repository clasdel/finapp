package com.example.finapp;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.finapp.db.DbUsuarios;
import com.example.finapp.entidades.Usuarios;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class EditarDatosCuenta extends AppCompatActivity {

    EditText txtNombre, txtApellido, txtCorreo, txtPass;
    Button btnGuardar, btnCancelar;
    FloatingActionButton fbEditar,fbEliminar;
    Boolean correcto;

    Usuarios usuario;
    int id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_perfiles);

        txtNombre = findViewById(R.id.edtNombre);
        txtApellido = findViewById(R.id.edtApellido);
        txtCorreo = findViewById(R.id.edtCorreo);
        txtPass = findViewById(R.id.edtPassword);
        btnGuardar = findViewById(R.id.btnEdGuardarCambios);
        btnCancelar = findViewById(R.id.btnEdCancelarCambios);
        fbEditar = findViewById(R.id.fabEditar);
        fbEliminar = findViewById(R.id.fabEliminar);


        if (savedInstanceState == null){
            Bundle extras = getIntent().getExtras();

            if (extras == null){
                id = Integer.parseInt(null);
            }else {
                id = extras.getInt("ID");
            }
        }else {
            id = (int) savedInstanceState.getSerializable("ID");
        }

        DbUsuarios dbUsuarios = new DbUsuarios(EditarDatosCuenta.this);
        usuario = dbUsuarios.verUsuario(id);

        if (usuario != null){
            txtNombre.setText(usuario.getNombre());
            txtApellido.setText(usuario.getApellido());
            txtCorreo.setText(usuario.getCorreo());
            txtPass.setText(usuario.getCorreo());
            fbEditar.setVisibility(View.INVISIBLE);
            fbEliminar.setVisibility(View.INVISIBLE);
        }

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nomb = txtNombre.getText().toString();
                String ape = txtApellido.getText().toString();
                String corre = txtCorreo.getText().toString();
                String contra = txtPass.getText().toString();

                if(!nomb.equals("") && !ape.equals("") && !corre.equals("") && !contra.equals("")){
                dbUsuarios.editaUsuario(id, txtNombre.getText().toString(),
                        txtApellido.getText().toString(),
                        txtCorreo.getText().toString(),
                        txtPass.getText().toString());
                    Toast.makeText(EditarDatosCuenta.this, "Registro actualizado correctamente", Toast.LENGTH_LONG).show();
                    verRegistro();
                }else{
                    Toast.makeText(EditarDatosCuenta.this, "No se puede editar, hay campos vacíos", Toast.LENGTH_LONG).show();
                }
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View cancelarView) {
                cancelarRegistro();
            }
        });

    }

    public void verRegistro(){
        Intent intent = new Intent(this, EditarPerfiles.class);
        intent.putExtra("ID", id);
        startActivity(intent);
    }

    public void cancelarRegistro(){
        Intent cancelar = new Intent(this,EditarPerfiles.class);
        cancelar.putExtra("ID", id);
        startActivity(cancelar);
    }

}