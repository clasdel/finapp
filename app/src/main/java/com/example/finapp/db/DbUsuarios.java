package com.example.finapp.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.Nullable;

import com.example.finapp.Consultar;
import com.example.finapp.entidades.Usuarios;

import java.util.ArrayList;

public class DbUsuarios extends DbHelper{

    Context context;


    public DbUsuarios(@Nullable Context context) {
        super(context);
        this.context = context;

    }

    public long insertarUsuario(String nombre, String apellido, String correo_electronico , String password){

        //funciones para insertar registro en la base de datos
        long id = 0;

        try {
            DbHelper dbHelper = new DbHelper(context);
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("nombre" , nombre);
            values.put("apellido" , apellido);
            values.put("correo_electronico" ,correo_electronico);
            values.put("password" , password);

            id = db.insert(TABLE_USUARIOS, null, values);

        } catch (Exception e) {
            e.toString();
        }
        return id;
    }

    public ArrayList<Usuarios> mostrarUsuarios(){
        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ArrayList<Usuarios> listaUsuarios = new ArrayList<>();
        Usuarios usuario = null;
        Cursor cursorUsuarios = null;

        cursorUsuarios = db.rawQuery("select * from " + TABLE_USUARIOS, null);

        if (cursorUsuarios.moveToFirst()){
            do {
                usuario = new Usuarios();
                usuario.setId(cursorUsuarios.getInt(0));
                usuario.setNombre(cursorUsuarios.getString(1));
                usuario.setApellido(cursorUsuarios.getString(2));
                usuario.setCorreo(cursorUsuarios.getString(3));
                usuario.setPassword(cursorUsuarios.getString(4));
                listaUsuarios.add(usuario);

            }while (cursorUsuarios.moveToNext());
        }
        cursorUsuarios.close();
        return listaUsuarios;

    }


    public Usuarios verUsuario(int id){
        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Usuarios usuario = null;
        Cursor cursorUsuarios = null;

        cursorUsuarios = db.rawQuery("SELECT * FROM " + TABLE_USUARIOS + " WHERE id = " + id + " limit 1", null);
        if (cursorUsuarios.moveToFirst()){
                usuario = new Usuarios();
                usuario.setId(cursorUsuarios.getInt(0));
                usuario.setNombre(cursorUsuarios.getString(1));
                usuario.setApellido(cursorUsuarios.getString(2));
                usuario.setCorreo(cursorUsuarios.getString(3));
                usuario.setPassword(cursorUsuarios.getString(4));
            }
        cursorUsuarios.close();
        return usuario;

    }

    public boolean editaUsuario(int id, String nombre, String apellido, String correo, String password){
        boolean correcto = false;
        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        try {
            db.execSQL("UPDATE " + TABLE_USUARIOS + " SET nombre = '"+ nombre +"'," +
                    " apellido = '"+ apellido +"'," +
                    " correo_electronico = '"+ correo +"'," +
                    " password = '"+ password +"' WHERE id = '"+ id +"' ");
        }catch (Exception e){
            e.toString();
            correcto = false;
        }finally {
            db.close();
        }
        return correcto;
    }

    public boolean eliminarUsuario(int id){
        boolean correcto = false;
        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        try {
            db.execSQL(" DELETE FROM " + TABLE_USUARIOS + " WHERE id = '"+ id +"'");
        }catch (Exception e){
            e.toString();
            correcto = false;
        }finally {
            db.close();
        }
        return correcto;
    }

    public int login(String c, String p){
        int a = 0;
        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Cursor cursorUsuarios = db.rawQuery("select * from " + TABLE_USUARIOS, null);

        if (cursorUsuarios != null && cursorUsuarios.moveToFirst()){
            do {
                if(cursorUsuarios.getString(3).equals(c) && cursorUsuarios.getString(4).equals(p)){
                    a++;
                }
            }while (cursorUsuarios.moveToNext());
        }
        return a;
    }

    public Usuarios obtenerUsuario(String us, String pas){
        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ArrayList<Usuarios> listaUsuarios = mostrarUsuarios();
        for (Usuarios u: listaUsuarios){
            if (u.getCorreo().equals(us) && u.getPassword().equals(pas)){
                return u;
            }
        }
        return null;
    }

    public Usuarios obtenerUsuarioPorId(int id){
        DbHelper dbHelper = new DbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ArrayList<Usuarios> listaUsuarios = mostrarUsuarios();
        for (Usuarios u: listaUsuarios){
            if (u.getId() == id){
                return u;
            }
        }
        return null;
    }

}
