package com.example.finapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class RegistrarSedes extends AppCompatActivity {

    Button bRegBan, bCanBan;
    EditText tBan, tDir, tCiudad, tBarrio;
    private FirebaseFirestore myFirestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_sedes);

        tBan = findViewById(R.id.txtRegNomBanc);
        tDir = findViewById(R.id.txtRegDir);
        tCiudad = findViewById(R.id.txtRegCity);
        tBarrio = findViewById(R.id.txtRegBarrio);
        bRegBan = findViewById(R.id.btnRegRegistrarBanco);
        bCanBan = findViewById(R.id.btnRegRegistrarBanco);
        myFirestore = FirebaseFirestore.getInstance();

        bRegBan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nomBan = tBan.getText().toString();
                String dirBan = tDir.getText().toString();
                String ciuBan = tCiudad.getText().toString();
                String barrBan = tBarrio.getText().toString();

                if (nomBan.isEmpty() && dirBan.isEmpty() && ciuBan.isEmpty() && barrBan.isEmpty()){
                    Toast.makeText(RegistrarSedes.this, "Algunos campos estan vacios ", Toast.LENGTH_LONG).show();
                }else {
                    postBanco(nomBan, dirBan, ciuBan, barrBan);
                }
            }
        });

    }

    private void postBanco(String nomBan, String dirBan, String ciuBan, String barrBan) {
        Map<String, Object> map = new HashMap<>();
        map.put("nombre", nomBan);
        map.put("direccion", dirBan);
        map.put("ciudad", ciuBan);
        map.put("barrio", barrBan);

        myFirestore.collection("Bancos").add(map).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                Toast.makeText(RegistrarSedes.this, "Banco Registrado", Toast.LENGTH_LONG).show();
                finish();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(RegistrarSedes.this, "Error al registrar", Toast.LENGTH_LONG).show();
            }
        });
    }

}