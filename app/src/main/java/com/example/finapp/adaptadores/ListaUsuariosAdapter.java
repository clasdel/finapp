package com.example.finapp.adaptadores;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finapp.EditarPerfiles;
import com.example.finapp.R;
import com.example.finapp.entidades.Usuarios;

import java.util.ArrayList;

public class ListaUsuariosAdapter extends RecyclerView.Adapter<ListaUsuariosAdapter.UsuarioViewHolder> {

    ArrayList<Usuarios> listaUsuarios;

    public ListaUsuariosAdapter(ArrayList<Usuarios> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    @NonNull
    @Override
    public ListaUsuariosAdapter.UsuarioViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vista_usuarios,null,false);
        return new UsuarioViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListaUsuariosAdapter.UsuarioViewHolder holder, int position) {

        holder.viewNombre.setText(listaUsuarios.get(position).getNombre());
        holder.viewApellido.setText(listaUsuarios.get(position).getApellido());
        holder.viewCorreo.setText(listaUsuarios.get(position).getCorreo());
        holder.viewPassword.setText(listaUsuarios.get(position).getPassword());

    }

    @Override
    public int getItemCount() {
        return listaUsuarios.size();
    }

    public class UsuarioViewHolder extends RecyclerView.ViewHolder {
        TextView viewNombre, viewApellido, viewCorreo, viewPassword;

        public UsuarioViewHolder(@NonNull View itemView) {
            super(itemView);
            viewNombre = itemView.findViewById(R.id.txtVisNombre);
            viewApellido = itemView.findViewById(R.id.txtVisApellidos);
            viewCorreo = itemView.findViewById(R.id.txtVisCorreo);
            viewPassword = itemView.findViewById(R.id.txtVisPassword);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Context context = view.getContext();
                    Intent intent = new Intent(context, EditarPerfiles.class);
                    intent.putExtra("ID", listaUsuarios.get(getAdapterPosition()).getId());
                    context.startActivity(intent);
                }
            });
        }
    }
}
